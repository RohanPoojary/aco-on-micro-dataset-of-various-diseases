The code runs Ant Colony Optimization for feature selection on Micro Dataset. The Dataset is of Colon, Embryonal, Leukamia and Lymphomia Diseases.

### How do I get set up? ###

* Install Numpy an Arff Module
* Change the output and Input Filename as Required      
      * _Input Filename_ - Any arff file from dataset folder.
      * _Output Filename_ - The output arff file that has the selected attributes and its data
* Then Run process.py