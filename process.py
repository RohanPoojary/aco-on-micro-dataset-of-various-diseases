#!/usr/bin/python2 -tt

import arff
import numpy as np
import pickle
import sys
import time


def getGeneSimilarity(data):
	output = np.identity(data.shape[1], )
	norms = np.linalg.norm(data, axis=0)
	for i in np.arange(len(output)):
		for j in np.arange(i+1, len(output)):
			sys.stdout.flush()
			sys.stdout.write("\rSimilarity of {}, {}".format(i, j))
			sum_of_products = np.sum(data[:, i] * data[:, j])
			sim = sum_of_products / (norms[i] * norms[j])
			output[i, j] = abs(sim)
			output[j, i] = abs(sim)
	print ""
	return output


def getGeneRelevance(data):
	# var = np.var(data, axis=0)
	# return var/data.shape[0]
	std = np.std(data, axis=0)
	avg = np.average(data, axis=0)
	exponent = - np.power(data - avg, 2) / (2 * np.power(std, 2))
	return np.var(np.power(np.e, exponent) / (std * np.sqrt(2 * np.pi)), axis=0) / data.shape[0]


def state_transition(init_pos, visited, pheromones, similarity):
	beta = 1
	heuristic = np.ones_like(similarity) * np.inf
	zeros = np.where(similarity[similarity == 0])
	if len(zeros[0]):
		print zeros
	heuristic[similarity != 0] = 1 / np.power(similarity[similarity != 0], beta)
	visited = visited[visited != -1]
	phero_sim = pheromones[init_pos, :] * heuristic[init_pos, :]
	phero_sim[visited] = 0
	next_post = np.argmax(phero_sim)
	return next_post


def fitness_func(ants, relev):
	fitness = np.array([], dtype=np.float128)
	k = ants.shape[1]
	for ant in ants:
		value = np.sum(relev[ant]) / k
		fitness = np.append(fitness, value)
	return fitness


def update_pheromones(pheromones, edge_counter, fitness_vals):
	rho = 0.2
	edge_counter = edge_counter / np.sum(edge_counter)
	present_pheromones = (1 - rho) * pheromones
	return present_pheromones + edge_counter + np.sum(fitness_vals)


def getSelectedGenes(data, max_iters, ant_count, gene_count):
	tot_genes = data.shape[1]
	#### Computes Gene Similarity ###
	filename = 'geneSimilarity-{}.pkl'.format(tot_genes)
	print "Computing Gene Similarity"
	try:
		sim = pickle.load(open(filename, 'rb'))
	except IOError:
		sim = getGeneSimilarity(data)
		f = open(filename, 'wb')
		pickle.dump(sim, f)
		f.close()
	print "Completed Gene Similarity"

	##### Compute relevance ####
	relev = getGeneRelevance(data)

	#### Initialize Pheromones ####
	init_pheromone_val = 0.2
	pheromones = np.ones_like(sim) * init_pheromone_val
	pheromones[np.arange(tot_genes), np.arange(tot_genes)] = 0
	best_solutions = np.empty((0, gene_count+1), dtype=np.float128)

	print "Starting Iterations"
	for t in np.arange(max_iters):
		#### Initialize Edge Counter ####
		edge_counter = np.zeros_like(sim)
		#### Initialize positions ###
		ants = np.ones((ant_count, gene_count), dtype=np.int) * -1
		positions = np.arange(tot_genes)
		np.random.shuffle(positions)
		ants[:, 0] = positions[:ant_count]
		for i in np.arange(gene_count):
			for k in np.arange(ant_count):
				sys.stdout.flush()
				sys.stdout.write('\rIn iteration: {}, Gen_cnt: {}, Ant: {}   '.format(t+1, i+1, k))
				new_position = state_transition(ants[k, i], ants[k], pheromones, sim)
				if i+1 < gene_count:
					ants[k, i+1] = new_position
				edge_counter[new_position, ants[k, i]] += 1
				edge_counter[ants[k, i], new_position] += 1
		fitness_val = fitness_func(ants, relev)
		best_fitness_ind = fitness_val.argmax()
		best_solutions = np.vstack((best_solutions, np.append(ants[best_fitness_ind], fitness_val[best_fitness_ind])))
		pheromones = update_pheromones(pheromones, edge_counter, fitness_val)
	print ""
	global_best_ind = np.argmax(best_solutions[:, -1])
	return best_solutions[global_best_ind, :-1].astype(np.int)


def main():
	##########    Input File    #######
	inp_file = 'Dataset/embryonal.arff'
	#########     Output File   #########
	out_file = 'new_lymphomia_dataset.arff'

	dataset = arff.load(open(inp_file, 'rb'))
	data = np.array(dataset['data'])
	data[data[:, -1] == '0', -1] = 0
	data[data[:, -1] == '1', -1] = 1
	data = data.astype(np.float128)
	max_iters = 3
	ant_cnt = 3
	gene_cnt = 10
	start = time.time()
	genes = getSelectedGenes(data[:, :-1], max_iters, ant_cnt, gene_cnt)
	print "Selected Genes"
	print genes
	stop = time.time()
	gap = stop - start
	print "Took {}:{}:{}".format(int(gap/3600), int(gap/60) % 60, int(gap) % 60)
	print "Dumping"
	import dumparff
	new_data = dumparff.dump(dataset, genes)
	f = open(out_file, 'wb')
	arff.dump(new_data, f)
	f.close()

if __name__ == '__main__':
	main()