#!/usr/bin/python2 -tt

import numpy as np

def dump(dataset, genes):
	data = np.array(dataset['data'])
	attributes = dataset['attributes']
	genes = np.append(genes, -1)
	output = dataset.copy()
	output['attributes'] = []
	output['data'] = data[:, genes]
	for gene in genes:
		output['attributes'].append(attributes[gene])
	output['data'] = output['data'].tolist()
	return output